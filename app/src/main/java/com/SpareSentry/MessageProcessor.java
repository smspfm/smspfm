package com.SpareSentry;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.github.mikephil.charting.data.Entry;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by App on 2018.07.13..
 */

public class MessageProcessor {
    private static final String TAG = "MessageProcessor";
    SavedData savedData;

    public MessageProcessor(Context context) {
        this.savedData = Tools.readSaveData(context);
        if(this.savedData == null){
            this.savedData = new SavedData();
        }
    }

    public void saveSaveData(Context context){
        Tools.writeSaveData(context, savedData);
    }

    public SavedData CollectDataFromSMS(Context context, Template template){
        savedData = new SavedData();
        return CollectDataFromSMS(context, template, savedData);
    }

    public SavedData CollectDataFromSMS(Context context, Template template, SavedData savedData) { //TODO saját objektum, ami megkapja a kursort, és feldolgozza
        Cursor cursor = Tools.getBankSmsCursor(context, template.PhoneNumbers);
        this.savedData = savedData;
        int maxSMS = 100; //TODO kivenni a maxot, vagy beállításként kezelni
        if (cursor.getCount() < maxSMS) { maxSMS = cursor.getCount(); }

        for (int idx = 1; idx <= maxSMS; idx++) {
            if (idx == 1){// Első iterációnál ellenőrízzük, hogy van-e üzenet
                try {
                    cursor.moveToFirst();
                } catch (Exception e) {
                    e.printStackTrace(); // TODO popup ablak harról, hogy nincs beolvasható üzenet, a banktól
                }
            } else {
                cursor.move(1); //TODO feldarabolni a stringet, és azt összehasonlítani a template-el
            }

            Long smsDate = Long.parseLong(cursor.getString(1));
            processMessage(getMessageType(cursor.getString(2), template),cursor.getString(2), smsDate, template) ;
        }
        return savedData;
    }

    public void processMessage(Field field, String messageText, Long messageDate, Template template) {
        Log.d(TAG,"Message process started");
        if (field != null) {
            SmsPart messageType = null;
            try {
                messageType = (SmsPart) field.get(template);
            } catch (IllegalAccessException e) {
                e.printStackTrace();//TODO logolás
            }
            Log.d(TAG,"Message type found: " +messageType.toString() );
            String[] substrings = messageText.split(" ");

            String pattern = "###,###.###";
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator(',');
            symbols.setGroupingSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols); //TODO templatból, message-ből megoldani

            try {
                String result = substrings[ Integer.parseInt(messageType.TrAmount.KeyWordPos)]; //TODO Tizedes elválasztó
                float trAmount = decimalFormat.parse(result, new ParsePosition(0)).floatValue();

                //Megkeressük a megjegyzés végét, ha a megjegyzés az üzenet közepén van.
                int endOfAnnouncement = substrings.length;
                for(Field f: messageType.getClass().getDeclaredFields()) {
                    if (f.getType() == Word.class) {
                        f.setAccessible(true);
                        try {
                            Word word = (Word) f.get(messageType);
                            if (Integer.parseInt(word.KeyWordPos) > Integer.parseInt(messageType.Announcement.KeyWordPos)
                                    && Integer.parseInt(word.KeyWordPos) < endOfAnnouncement) endOfAnnouncement = Integer.parseInt(word.KeyWordPos);
                        } catch (Exception e){
                            e.printStackTrace();//TODO logolás
                        }
                    }
                }

                String announcement = "";
                for(int i=Integer.parseInt(messageType.Announcement.KeyWordPos); i< endOfAnnouncement; i++){
                    announcement += substrings[i] + " ";
                }
                //TODO megcserélni a kiadás bevétel feldolgozást, bevételt könnyebb ellenőrízni, vagy több van belőlle?
                if(messageDate > Tools.getFirstDayOfMonth(0)){//TODO calendar helyet date-et használni
                    //ha kiadás volt
                    if(field.equals(Template.class.getField("Debit"))  //field.equals(Template.class.getField("Transfer")) || //külön jön terheléses üzenet is, nem kell ezzel külön foglalkozni
                            || field.equals(Template.class.getField("Buy"))){
                        savedData.monthlyExpense += trAmount; //TODO esetleg a legvégén a textView-ekből összegyűjteni
                        savedData.list_monthlyExpenses.add(new MessageData(messageDate, trAmount, announcement));
                        if(savedData.lastExpenseDate == null || new SimpleDateFormat().parse(savedData.lastExpenseDate).getTime() < messageDate){ //TODO a legújjabat figyelembe venni || vagy kissebb
                            savedData.lastExpenseDate = new SimpleDateFormat().format(new Date(messageDate));
                        }
                    //ha bevétel volt
                    } else if (field.equals(Template.class.getField("Credit"))){
                        savedData.monthlyIncome += trAmount; //TODO bevétel összetételéről is diagram, esetleg kikapcsolható beállításokban
                        savedData.list_monthlyIncomes.add(new MessageData(messageDate, trAmount, announcement));
                        if(savedData.lastIncomeDate == null || new SimpleDateFormat().parse(savedData.lastIncomeDate).getTime() < messageDate){
                            savedData.lastIncomeDate = new SimpleDateFormat().format(new Date(messageDate));
                        }
                    }
                //Előző hónapban érkezett üzenetek feldolgozása
                } else if(messageDate > Tools.getFirstDayOfMonth(-1)){
                    if(field.equals(Template.class.getField("Debit")) //ha kiadás volt
                            || field.equals(Template.class.getField("Buy"))){
                        savedData.monthlyExpenseLastMonth += trAmount;
                        savedData.list_lastMonthExpenses.add(new MessageData(messageDate, trAmount, announcement));

                    } else if (field.equals(Template.class.getField("Credit"))){
                        savedData.monthlyIncomeLastMonth += trAmount;
                        savedData.list_lastMonthIncomes.add(new MessageData(messageDate, trAmount, announcement));
                    }
                } else {
                    if(field.equals(Template.class.getField("Debit")) //ha kiadás volt
                            || field.equals(Template.class.getField("Buy"))){
                        savedData.list_oldExpenses.add(new MessageData(messageDate, trAmount, announcement));

                    } else if (field.equals(Template.class.getField("Credit"))){
                        savedData.list_oldIncomes.add(new MessageData(messageDate, trAmount, announcement));
                    }
                }
                Log.d(TAG,"Message processed." );//TODO currency hozzáadása

                //Összagyűjtjük az összes egyenleg összeget, hogy grafikont lehessen belőlle rajzolni
                try {
                    String balanceResult = substrings[ Integer.parseInt(messageType.Balance.KeyWordPos)];
                    savedData.entries_balance.add(new Entry(messageDate, decimalFormat.parse(balanceResult, new ParsePosition(0)).floatValue()));
                    //A legelső, tehát legfrisebb egyenleg összeget tároljuk
                    if(savedData.balance == 0.0f ||  Long.parseLong(savedData.lastIncomeDate) < messageDate && Long.parseLong(savedData.lastExpenseDate) < messageDate ) {
                        savedData.balance = decimalFormat.parse(balanceResult, new ParsePosition(0)).floatValue();
                    }
                }catch (Exception e) {
                    Log.d(TAG,"Something bad happened:" + e.getMessage() );
                    e.printStackTrace(); //TODO logolás, itt még szokott lenni hiba
                }
            } catch (Exception e) {
                Log.d(TAG,"Something bad happened:" + e.getMessage() );
                e.printStackTrace();//TODO logolás, itt még szokott lenni hiba
            }
            //TODO savedata mentése fájlba
        }
    }

    public static Field getMessageType(String string, Template template) {
        Log.d(TAG, "GetMessageType: " + string);
        String[] substrings = string.split(" ");
        for(Field f: template.getClass().getDeclaredFields()){
            if(f.getType() == SmsPart.class) {
                f.setAccessible(true);
                try {
                    SmsPart smsPart = (SmsPart) f.get(template);
                    String normalizedMessageKeyword = null, normalizedTemplateKeyword = null;
                    if(smsPart.TrType.Text != null && substrings[Integer.parseInt(smsPart.TrType.KeyWordPos)] != null){
                        normalizedTemplateKeyword = Tools.normalize(smsPart.TrType.Text);

                        int spaceCount = normalizedTemplateKeyword.length() - normalizedTemplateKeyword.replace(" ", "").length(); //TODO szóköz helyett delimiter használata

                        //Ha a templatben a kulcsszó nem egyszavas
                        normalizedMessageKeyword = substrings[Integer.parseInt(smsPart.TrType.KeyWordPos)];
                        if(spaceCount > 0){
                            for (int i = 1; i <= spaceCount; i++) {
                                normalizedMessageKeyword += " " + substrings[Integer.parseInt(smsPart.TrType.KeyWordPos) + i];
                            }
                        }

                        normalizedMessageKeyword =  Tools.normalize(normalizedMessageKeyword);
                    }

                    if (normalizedMessageKeyword.equals(normalizedTemplateKeyword)){
                        return f;
                    }
                } catch (Exception e) {
                    e.printStackTrace(); //TODO logolás
                    Log.d(TAG, "getMessageType " + e);
                }
            }
        }
        return null;
    }
}