package com.SpareSentry;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app.smspfm.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;


public class WelcomeActivity extends AppCompatActivity {

    private int MY_RESULT_CODE_CONTACT_PICK = 1;
    private int MY_PERMISSIONS_READ_CONTACTS = 2;
    Template template;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        TextView textView = findViewById(R.id.tvWelcome);
        textView.setText(Html.fromHtml(getResources().getString(R.string.welcomeText)));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;

        ImageView imageView = findViewById(R.id.imageView);
/*
        int imageHeight = (int) (height - (height/ 1.618)); //733
        imageView.getLayoutParams().height= imageHeight;
        imageView.requestLayout();
*/
        Button agree = findViewById(R.id.agree);
        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(WelcomeActivity.this,
                        Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(WelcomeActivity.this,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            MY_PERMISSIONS_READ_CONTACTS);
                } else {
                    Intent pickContactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                    startActivityForResult(pickContactIntent, MY_RESULT_CODE_CONTACT_PICK);
                }
            }
        });

        Button later = findViewById(R.id.button_Later);
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.writeSaveData(getApplicationContext(), new SavedData());
                Intent main = new Intent(getBaseContext(), MainActivity.class);
                main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(main);
            }
        });
    }

    @Override
    public void onActivityResult( int requestCode, int resultCode, Intent intent ) {

        super.onActivityResult( requestCode, resultCode, intent );
        if ( requestCode == MY_RESULT_CODE_CONTACT_PICK) {

            if ( resultCode == RESULT_OK )
            {
                Uri contactData = intent.getData();
                Cursor cursor = getBaseContext().getContentResolver().query(contactData, null,null, null, null);

                String lookupKey = "";
                String name = "";
                List<String> phoneNumbers = new ArrayList<>();

                if (cursor.isBeforeFirst()){
                    if (cursor.moveToFirst()){ // must check the result to prevent exception
                        lookupKey = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LOOKUP_KEY));
                        name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    }
                }

                Cursor phoneCursor = getBaseContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.LOOKUP_KEY + " = ?", new String[]{lookupKey}, null);
                if (phoneCursor.moveToFirst()){
                    phoneNumbers.add( phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)));
                }
                while (phoneCursor.moveToNext()) {
                    phoneNumbers.add( phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)));
                }

                goToMessagePickerActivity(phoneNumbers, name, lookupKey);

                /*
                if(isOnline()) { //TODO ablak, amiben kérjük, hogy legyen internetkapcsolat, majd újra próbálkozni
                    Toast.makeText(getBaseContext(), "Minta letöltése elkezdődött.",//"Template download started", //TODO kitenni strings.xml-be
                            Toast.LENGTH_SHORT).show();
                    getTemplate(phoneNumbers, name, lookupKey);
                } else {
                    Toast.makeText(getBaseContext(), "Minta letöltése elkezdődött.",//"Template download failed",
                            Toast.LENGTH_SHORT).show();
                    goToMessagePickerActivity(phoneNumbers, name, lookupKey);
                }
                */
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_READ_CONTACTS){ //TODO Ha nem kaptuk meg az engedélyt, akkor egy ablak, hogy miért kell, és újra megpróbáltatni
            Intent pickContactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(pickContactIntent, MY_RESULT_CODE_CONTACT_PICK);
        }
    }

    private void getTemplate(final List<String> phoneNumbers, final String name, final String id) {
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference("data/" + phoneNumbers.get(0));
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    template = dataSnapshot.getValue(Template.class);
                    /*bankName = dataSnapshot.getValue(String.class);*/
                    Gson gson = new Gson();
                    String json = gson.toJson(template);
                    FileOutputStream outputStream;
                    try {
                        outputStream = openFileOutput("Template.cls", Context.MODE_PRIVATE);
                        outputStream.write(json.getBytes());
                        outputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getBaseContext(), "Minta letöltése befejeződött.", Toast.LENGTH_SHORT).show();//Template downloaded
                    Intent MainActivityIntent = new Intent(getBaseContext(), MainActivity.class);
                    MainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(MainActivityIntent);
                } else {
                    Toast.makeText(getBaseContext(), "Minta letöltése nem sikerült.", Toast.LENGTH_SHORT).show();//Template download failed
                    goToMessagePickerActivity(phoneNumbers, name, id);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), "Template download failed",
                        Toast.LENGTH_SHORT).show();
                goToMessagePickerActivity(phoneNumbers, name, id);
            }
        });
    }


    public void goToMessagePickerActivity(final List<String> phoneNumbers, final String name, final String id){
        DataViewModel dataViewModel;
        dataViewModel = ViewModelProviders.of(this).get(DataViewModel.class);
        dataViewModel.phoneNumber.setValue(phoneNumbers);

        Intent messagePicker = new Intent(getBaseContext(), MessagePickerActivity.class);
        messagePicker.putExtra("phoneNumber", (Serializable) phoneNumbers);
        messagePicker.putExtra("bankName", name);
        messagePicker.putExtra("id", id);
        startActivity(messagePicker);
    }

    public String formatPhoneNumber(final String number){
        String phoneNum = number;
        TelephonyManager manager = (TelephonyManager)getSystemService(this.TELEPHONY_SERVICE);
        String ISO2 = manager.getSimCountryIso();
        phoneNum = PhoneNumberUtils.formatNumber(phoneNum, ISO2).replace(" ", ""); //TODO try catch ha back-el visszamennek, és nem választanak ki semmit
        final String finalPhoneNum = phoneNum;
        return finalPhoneNum;
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}