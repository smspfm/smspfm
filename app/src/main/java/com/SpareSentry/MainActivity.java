package com.SpareSentry;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app.smspfm.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

public class MainActivity extends AppCompatActivity implements DialogAddTransaction.AddTransactionDialogListener {
    Context context;
    float monthlyExpense = 0.0f;
    float monthlyExpenseLastMonth = 0.0f;
    float balance = 0.0f;
    float monthlyIncome = 0.0f;
    float monthlyIncomeLastMonth = 0.0f;
    int selectedSliceViewId = -1;
    List<Entry> entries_balance = new ArrayList<>();
    SavedData savedData;
    Switch switch_ShowNums;
    Template template;
    TextView textView_Balance, tvIncome, tvExpense, tvachievement, tvIncomePerDay,tvExpensePerDay, tvIncomeDate, tvExpenseDate, tvLMIncome, tvLMExpense;

    private static final int MY_PERMISSIONS_READ_SMS = 1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
    }
    private void GetAndProcessSaveData() {
        MessageProcessor messageProcessor = new MessageProcessor(context);
        savedData = messageProcessor.CollectDataFromSMS(context, template);
        Tools.writeSaveData(context, savedData);
        entries_balance = savedData.entries_balance;
        balance = savedData.balance;
        monthlyIncome = savedData.monthlyIncome;
        monthlyExpense = savedData.monthlyExpense;
        monthlyIncomeLastMonth = savedData.monthlyIncomeLastMonth;
        monthlyExpenseLastMonth = savedData.monthlyExpenseLastMonth;

        PopulateAndSetUpViews();
        tvIncomeDate.setText(savedData.lastIncomeDate);
        tvExpenseDate.setText(savedData.lastExpenseDate);

        SetProgressBars();
    }

    @Override
    protected void onResume() {
        super.onResume();

        DataViewModel dataViewModel;
        dataViewModel = ViewModelProviders.of(this).get(DataViewModel.class);


        savedData = Tools.readSaveData(context);
        if(savedData != null){
            entries_balance = savedData.entries_balance;
            balance = savedData.balance;
            monthlyIncome = savedData.monthlyIncome;
            monthlyExpense = savedData.monthlyExpense;
            monthlyIncomeLastMonth = savedData.monthlyIncomeLastMonth;
            monthlyExpenseLastMonth = savedData.monthlyExpenseLastMonth;

            setContentView(R.layout.activity_main);
            PopulateAndSetUpViews();
            tvIncomeDate.setText(savedData.lastIncomeDate);
            tvExpenseDate.setText(savedData.lastExpenseDate);

            AppSettings appSettings = Tools.readAppsettings(context);
            if (appSettings.SwitchPos) { //TODO esetleg optimalizálni, azzal, hogy összenézzük a kapcsoló jelenlegi helyzetével
                switch_ShowNums.setChecked(true);
            } else {
                switch_ShowNums.setChecked(false);
            }

            SetProgressBars();
        } else {
            template = Tools.readTemplate(context);

            if (template == null) {
                Intent welcome = new Intent(this, WelcomeActivity.class);
                welcome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(welcome);
            } else {
                setContentView(R.layout.activity_main);
                if (ContextCompat.checkSelfPermission(getBaseContext(),
                        android.Manifest.permission.READ_SMS)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS},
                            MY_PERMISSIONS_READ_SMS);
                } else {
                    GetAndProcessSaveData();
                    PopulateAndSetUpViews();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Át kell tenni korábra
        /* //Az alábbiak azért kellenének, hogyha kilépünk az alkalmazásból, akkor elrejtsük a számokat. De így képernyő forgatáskor is elrejtjük.
        AppSettings appSettings = Tools.readAppsettings(context);
        if (appSettings == null) appSettings = new AppSettings();
        appSettings.SwitchPos = false;
        Tools.writeAppsettings(context, appSettings);*/
    }

    private void ShowNums() {
        String currency = "HUF"; //TODO templatből kivenni

        NumberFormat nf = NumberFormat.getInstance();
        textView_Balance.setText(nf.format(balance) + " " + currency);
        tvIncome.setText(nf.format(monthlyIncome) + " " + currency);
        tvExpense.setText(nf.format(monthlyExpense) + " " + currency);
        //tvachievement.setText(getResources().getString(R.string.achievement));
        Calendar rightNow = Calendar.getInstance();
        tvIncomePerDay.setText(nf.format( monthlyIncome / rightNow.getActualMaximum(Calendar.DAY_OF_MONTH)) + " HUF/ nap");
        tvExpensePerDay.setText(nf.format((monthlyExpense / rightNow.get(Calendar.DAY_OF_MONTH))) + " HUF/ nap");

        tvLMIncome.setText(nf.format(monthlyIncomeLastMonth) + " HUF");
        tvLMExpense.setText(nf.format(monthlyExpenseLastMonth) + " HUF");

        PieChart pieChart = findViewById(R.id.chart);
        PieDataSet pieDataSet = (PieDataSet) pieChart.getData().getDataSet();
        pieDataSet.setDrawValues(true);
        pieChart.invalidate();
    }

    private void HideNums() {
        textView_Balance.setText(getResources().getString(R.string.amount));
        tvIncome.setText(getResources().getString(R.string.income));
        tvExpense.setText(getResources().getString(R.string.expense));

      //  tvachievement.setText(getResources().getString(R.string.achievement));
        tvIncomePerDay.setText(getResources().getString(R.string.incomePerDay));
        tvExpensePerDay.setText(getResources().getString(R.string.expensePerDay));

        tvLMIncome.setText(getResources().getString(R.string.amount));
        tvLMExpense.setText(getResources().getString(R.string.amount));

        PieChart pieChart = findViewById(R.id.chart);
        PieDataSet pieDataSet = (PieDataSet) pieChart.getData().getDataSet();
        pieDataSet.setDrawValues(false);
        pieChart.invalidate();
    }

    public void SetPieChart(){
        final PieChart pieChart = findViewById(R.id.chart);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        pieChart.setMinimumHeight((int) (width * 0.8f));

        List<PieEntry> monthlyEntries = new ArrayList<>();
        for(MessageData messageData : savedData.list_monthlyExpenses){
            monthlyEntries.add(new PieEntry(messageData.TrAmount, new EntryData(messageData.MessageDate, messageData.Announcement)));
        }

        PieDataSet set = new PieDataSet(monthlyEntries, getResources().getString(R.string.expense));
        PieData data = new PieData(set);
        set.setSliceSpace(3f);
        set.setColors(ColorTemplate.MATERIAL_COLORS);
        set.setValueTextSize(11f);
        set.setValueTextColor(Color.WHITE);
        set.setDrawValues(false);
        //set.setSliceSpace(0);
        pieChart.setDrawEntryLabels(false);
        pieChart.getLegend().setEnabled(false);
        pieChart.getDescription().setEnabled(false);

        pieChart.setData(data);
        pieChart.setRotationEnabled(false);
        pieChart.setCenterText(getResources().getString(R.string.expense));
        pieChart.setOnChartValueSelectedListener( new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {

                CardView card = new CardView(context);
                ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                        findViewById(R.id.cardView1).getWidth(),
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );
                card.setLayoutParams(params);

                ConstraintLayout constraintLayout = findViewById(R.id.constraintLayout);

                if(selectedSliceViewId < 0 ){
                    selectedSliceViewId = View.generateViewId();
                    card.setId(selectedSliceViewId);
                } else {
                    constraintLayout.removeView(findViewById(selectedSliceViewId));
                    card.setId(selectedSliceViewId);
                }

                TextView tv = new TextView(context);
                tv.setLayoutParams(params);
                EntryData entryData = (EntryData) e.getData();
                String amount = "";
                if (switch_ShowNums.isChecked()){
                    amount =  e.getY() + "\n";
                }
                tv.setText(amount +
                        entryData.Announcement +
                        "\n" + entryData.FormatedDate);
                tv.setTextAppearance(R.style.TextAppearance_AppCompat_Medium);

                card.addView(tv);
                card.setUseCompatPadding(true);
                //card.setPadding(8,0,8,0); //TODO a belső térközt megoldani
                //card.setContentPadding(8,0,8,0);

                constraintLayout.addView(card);
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(constraintLayout);
                constraintSet.connect(card.getId(), ConstraintSet.TOP, pieChart.getId(), ConstraintSet.BOTTOM, 8);
                constraintSet.centerHorizontally(card.getId(), constraintLayout.getId());
                constraintSet.applyTo(constraintLayout);
                //constraintLayout.scrollTo(0, 1);
                card.getParent().requestChildFocus(card,card);
            }

            @Override
            public void onNothingSelected() {

            }
        });
        pieChart.invalidate();
    }

    public void SetProgressBars(){
        ProgressBar incomeBar = findViewById(R.id.incomeBar);
        ProgressBar expenseBar = findViewById(R.id.expenseBar);

        if(monthlyIncome > monthlyExpense){
            incomeBar.setProgress(100);
            expenseBar.setProgress( Math.round((monthlyExpense /monthlyIncome)*100));
        } else {
            expenseBar.setProgress(100);
            incomeBar.setProgress( Math.round((monthlyIncome/ monthlyExpense)*100));
        }
        incomeBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#4CAF50")));
        expenseBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#F44336")));

        //Előző hónap progressBar-jai
        ProgressBar incomeBarLastMonth = findViewById(R.id.incomeBarLastMonth);
        ProgressBar expenseBarLastMonth = findViewById(R.id.expenseBarLastMonth);

        if(monthlyIncomeLastMonth > monthlyExpenseLastMonth){
            incomeBarLastMonth.setProgress(100);
            expenseBarLastMonth.setProgress( Math.round((monthlyExpenseLastMonth /monthlyIncomeLastMonth)*100));
        } else {
            expenseBarLastMonth.setProgress(100);
            incomeBarLastMonth.setProgress( Math.round((monthlyIncomeLastMonth/ monthlyExpenseLastMonth)*100));
        }
        incomeBarLastMonth.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#4CAF50")));
        expenseBarLastMonth.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#F44336")));

    }

    public void PopulateAndSetUpViews(){
        tvIncome = findViewById(R.id.tvIncome);
        tvExpense = findViewById(R.id.tvExpense);
        //tvachievement = findViewById(R.id.achievement);
        tvIncomeDate = findViewById(R.id.tvIncomeDate);
        tvExpenseDate = findViewById(R.id.tvExpenseDate);
        tvIncomePerDay = findViewById(R.id.incomePerDay);
        tvExpensePerDay= findViewById(R.id.expensePerDay);
        tvLMIncome = findViewById(R.id.tvLMIncome);
        tvLMExpense = findViewById(R.id.tvLMExpense);

        FloatingActionButton floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO dialog, új adat hozzáadásához SaveDate-hoz
                DialogAddTransaction addTransactionDialog = new DialogAddTransaction();
                addTransactionDialog.show(getSupportFragmentManager(),"AddTransactionDialog");
            }
        });

        //A SetPieChart-nak itt kell lennie, mert a showNums/ HideNums használja
        SetPieChart();
        switch_ShowNums = findViewById(R.id.switch1);
        switch_ShowNums.setChecked(false);
        switch_ShowNums.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(switch_ShowNums.isChecked()) {
                    ShowNums();
                    Tools.switchPosToFile(context, true);
                } else {
                    HideNums();
                    Tools.switchPosToFile(context, false);
                }
            }
        });

        Button settingsButton = findViewById(R.id.button_settings);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent settings = new Intent(getBaseContext(), SettingsActivity.class);
                startActivity(settings);
            }
        });

        //TODO az alábbi kettöt összevonni onclickListenert
        textView_Balance = findViewById(R.id.tvBalance);
        textView_Balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!entries_balance.isEmpty()) {
                    Intent graph = new Intent(view.getContext(), GraphActivity.class); //A textView_balanceAmount OnClickListener-jével együtt kezelendő
                    Gson gson = new Gson();
                    graph.putExtra("lineChart", true);
                    graph.putExtra("entries", gson.toJson(entries_balance));
                    graph.putExtra("showNums", switch_ShowNums.isChecked());
                    startActivity(graph);
                }else{
                    Toast.makeText(getBaseContext(), "Nincs megjeleníthető adat.", //TODO kitenni strings.xml-be
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        TextView textView_balanceAmount = findViewById(R.id.textView_balanceAmount);
        textView_balanceAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!entries_balance.isEmpty()) {
                    Intent graph = new Intent(view.getContext(), GraphActivity.class);
                    Gson gson = new Gson();
                    graph.putExtra("lineChart", true);
                    graph.putExtra("entries", gson.toJson(entries_balance));
                    graph.putExtra("showNums", switch_ShowNums.isChecked());
                    startActivity(graph);
                }else{
                    Toast.makeText(getBaseContext(), "Nincs megjeleníthető adat.", //TODO kitenni strings.xml-be
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.cardView1).setOnClickListener(new View.OnClickListener() { //Bevételek cardView
            @Override
            public void onClick(View view) {
                if(!savedData.list_monthlyIncomes.isEmpty()){
                    Intent graph = new Intent(view.getContext(),GraphActivity.class);
                    Gson gson = new Gson();
                    graph.putExtra("lineChart",false);
                    graph.putExtra("entries",gson.toJson(Tools.messageDataToBarEntry(savedData.list_monthlyIncomes)));
                    graph.putExtra("showNums", switch_ShowNums.isChecked());
                    startActivity(graph);
                }else{
                    Toast.makeText(getBaseContext(), "Nincs megjeleníthető adat.", //TODO kitenni strings.xml-be
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.cardView2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!savedData.list_monthlyExpenses.isEmpty()) {
                    Intent graph = new Intent(view.getContext(), GraphActivity.class);
                    Gson gson = new Gson();
                    graph.putExtra("lineChart", false);
                    graph.putExtra("entries", gson.toJson(Tools.messageDataToBarEntry(savedData.list_monthlyExpenses)));
                    graph.putExtra("showNums", switch_ShowNums.isChecked());
                    startActivity(graph);
                }else{
                    Toast.makeText(getBaseContext(), "Nincs megjeleníthető adat.", //TODO kitenni strings.xml-be
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void AddDialogFinish(DialogAddTransaction dialogFragment) {
        onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults)
    {
        if (requestCode == MY_PERMISSIONS_READ_SMS) {
            GetAndProcessSaveData();
        }//TODO Ha nem kaptuk meg az engedélyt, akkor egy ablak, hogy miért kell, és újra megpróbáltatni
    }
}