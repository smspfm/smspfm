package com.SpareSentry;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DataViewModel extends ViewModel {
    public MutableLiveData<List<String>> message = new MutableLiveData();
    public MutableLiveData<List<String>> output = new MutableLiveData();
    public MutableLiveData<List<String>> phoneNumber = new MutableLiveData<>();
}
