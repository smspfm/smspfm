package com.SpareSentry;

import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony;
import android.text.TextUtils;

import com.github.mikephil.charting.data.BarEntry;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class Tools {

    public static String normalize(String string){
        return  Normalizer
                .normalize(string, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
    }

    public static List<String> getCurrencys(){ //TODO kiegészíteni a nemzetközi jelölésekkel, fájlból beolvasni, vagy cloud-ból
        return new ArrayList<>( Arrays.asList("HUF", "USD"));
    }

    public static void writeSaveData(Context context, SavedData savedData) {
        String filename = "SavedData.cls";
        savedData.lastModified = Calendar.getInstance().get(Calendar.MILLISECOND);
        FileOutputStream outputStream;
        try {
            Gson gson = new Gson();
            outputStream = context.openFileOutput(filename, context.MODE_PRIVATE);
            outputStream.write(gson.toJson(savedData).getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();//TODO logolás
        }
    }

    public static SavedData readSaveData(Context context) {
        String filename = "SavedData.cls";
        String line = new String();
        String text = new String();
        FileInputStream inputStream;
        try {
            inputStream = context.openFileInput(filename);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            inputStreamReader.close();
            text = stringBuilder.toString();

        } catch (Exception e) {
            e.printStackTrace(); //TODO logolás
        }

        if ( text.length() > 0) {
            SavedData savedData = new Gson().fromJson(text, SavedData.class);
            //Ha már új hónap van, az előző hónap adatait átmozgatjuk
            if(savedData.lastModified < Tools.getFirstDayOfMonth(0) ){
                savedData.list_oldExpenses.addAll(savedData.list_lastMonthExpenses);
                savedData.list_lastMonthExpenses.clear();
                savedData.list_oldIncomes.addAll(savedData.list_lastMonthIncomes);
                savedData.list_lastMonthIncomes.clear();
                //Nem tudjuk melyek az új, melyek a régi hónap üzenetei, ezért szétválogatjuk
                for(MessageData messageData : savedData.list_monthlyExpenses) {
                    if (messageData.MessageDate < Tools.getFirstDayOfMonth(0)) {
                        savedData.list_lastMonthExpenses.add(messageData);
                        savedData.list_monthlyExpenses.remove(messageData);
                    }
                }

                for(MessageData messageData : savedData.list_monthlyIncomes) {
                    if (messageData.MessageDate < Tools.getFirstDayOfMonth(0)) {
                        savedData.list_lastMonthIncomes.add(messageData);
                        savedData.list_monthlyIncomes.remove(messageData);
                    }
                }
                savedData.lastModified = Calendar.getInstance().get(Calendar.MILLISECOND);
            }
            return savedData;
        }
        else return null;
    }

    public static Long getFirstDayOfMonth(int month){
        Calendar calendar = Calendar.getInstance();   // this takes current date
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + month);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static Cursor getBankSmsCursor(Context context, List<String> phoneNumbers) { //TODO _ID alapján keresni az sms-eket
        ContentResolver resolver = context.getContentResolver();
        Uri inboxUri = Telephony.Sms.Inbox.CONTENT_URI;
        String[] projection = new String[] { Telephony.Sms.Inbox.ADDRESS, Telephony.Sms.Inbox.DATE, Telephony.Sms.Inbox.BODY, Telephony.Sms.Inbox._ID };
        String[] selectionArgs = new String[phoneNumbers.size()];
        phoneNumbers.toArray(selectionArgs);
        return resolver.query(inboxUri, projection, Telephony.Sms.Inbox.ADDRESS + " in (" + TextUtils.join(",", Collections.nCopies(phoneNumbers.size(), "?")) + ")", selectionArgs,Telephony.Sms.Inbox.DATE + " DESC"); //TODO miért érkezik ide kétszer a végrehajtás?
    }

    public static void uploadTemplate(Template template) {
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        for (String phoneNumber : template.PhoneNumbers) {
            mDatabase.child("data").child(phoneNumber).setValue(template);
        }
    }

    public static Template readTempTemplate(Context context) {
        return readTemplateFile(context, "tmpTemplate.cls");
    }

    public static Template readTemplate(Context context) {
        return readTemplateFile(context, "Template.cls");
    }

    private static Template readTemplateFile(Context context, String filename ) {
        String line = new String();
        String text = new String();
        FileInputStream inputStream;
        try {
            inputStream = context.openFileInput(filename);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            inputStreamReader.close();
            text = stringBuilder.toString();

        } catch (Exception e) {
            e.printStackTrace(); //TODO logolás
        }

        if ( text.length() > 0) {
            Gson gson = new Gson();
            return gson.fromJson(text, Template.class);
        }
        else return null;
    }

    public static void writeTempTemplate(Context context, Template template) {
        writeFile(context, template, "tmpTemplate.cls");
    }

    public static void writeTemplate(Context context, Template template) {
        writeFile(context, template, "Template.cls");
    }

    private static void writeFile(Context context, Object object, String filename) {
        //String filename = "Template.cls";
        FileOutputStream outputStream;
        try {
            Gson gson = new Gson();
            outputStream = context.openFileOutput(filename, context.MODE_PRIVATE);
            outputStream.write(gson.toJson(object).getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();//TODO logolás
        }
    }

    public String nextBankMessage(Cursor cursor) {
        if (cursor.isBeforeFirst()){ //Első sms beolvasása
            if (cursor.moveToFirst()){ // must check the result to prevent exception
                return cursor.getString(2);
            } // TODO popup ablak harról, hogy nincs beolvasható sms, a banktól
        } else if (!cursor.isLast()) {//ha nem az utolsó
            cursor.move(1);
            return cursor.getString(2);
        }
        return null;
    }

    public static List<BarEntry> messageDataToBarEntry(List<MessageData> list_monthlyIncomes) {
        if(list_monthlyIncomes != null){
            List<BarEntry> list_barEntry = new ArrayList<>();
            for (MessageData messageData : list_monthlyIncomes){
                list_barEntry.add(new BarEntry(messageData.MessageDate, messageData.TrAmount,messageData.Announcement));
            }
            return list_barEntry;
        }else return null;
    }

    public static AppSettings readAppsettings(Context context) {
        AppSettings appSettings = new AppSettings();
        String filename = "Settings.cls"; //TODO szövegfelolvasós metódus készítése
        String line = new String();
        String text = new String();
        FileInputStream inputStream;
        try {
            inputStream = context.openFileInput(filename);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            inputStreamReader.close();
            text = stringBuilder.toString();

        } catch (Exception e) {
            e.printStackTrace(); //TODO logolás
        }

        if (text.length() > 0) {
            Gson gson = new Gson();
            return gson.fromJson(text, AppSettings.class);
        } else {
            appSettings.SwitchPos = false;
            return appSettings;
        }
    }

    public static void writeAppsettings(Context context, AppSettings appSettings) {
        String filename = "Settings.cls";
        FileOutputStream outputStream;
        try {
            Gson gson = new Gson();
            outputStream = context.openFileOutput(filename, context.MODE_PRIVATE);
            outputStream.write(gson.toJson(appSettings).getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();//TODO logolás
        }
    }

    public static void switchPosToFile(Context context, boolean switchPos) {
        AppSettings appSettings = new AppSettings();
        String filename = "Settings.cls";
        String line = new String();
        String text = new String();
        FileInputStream inputStream;
        try {
            inputStream = context.openFileInput(filename);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            inputStreamReader.close();
            text = stringBuilder.toString();

        } catch (Exception e) {
            e.printStackTrace(); //TODO logolás
        }

        if ( text.length() > 0) {
            Gson gson = new Gson();
            appSettings = gson.fromJson(text, AppSettings.class);
        }
        appSettings.SwitchPos = switchPos;

        FileOutputStream outputStream;
        try {
            Gson gson = new Gson();
            outputStream = context.openFileOutput(filename, context.MODE_PRIVATE);
            outputStream.write(gson.toJson(appSettings).getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();//TODO logolás
        }
    }
}
