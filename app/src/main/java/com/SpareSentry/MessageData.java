package com.SpareSentry;

/**
 * Created by App on 2018.07.12..
 */

public class MessageData {
    Long MessageDate;
    float TrAmount;
    String Announcement;

    public MessageData(Long messageDate, float trAmount, String announcement) {
        this.MessageDate = messageDate;
        this.TrAmount =  trAmount;
        this.Announcement = announcement;
    }
}
