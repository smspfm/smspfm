package com.SpareSentry;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony;
import android.view.View;

/**
 * Created by App on 2018.02.14..
 */

public class SmsTool {

    public static Cursor GetBankSMSCursor(Context context, String phoneNum) //TODO _ID alapján keresni az sms-eket
    {
        ContentResolver resolver = context.getContentResolver();
        Uri inboxUri = Telephony.Sms.Inbox.CONTENT_URI;
        String[] projection = new String[] { Telephony.Sms.Inbox.ADDRESS, Telephony.Sms.Inbox.DATE, Telephony.Sms.Inbox.BODY };

        Cursor cursor = resolver.query(inboxUri, projection, Telephony.Sms.Inbox.ADDRESS + " = ?", new  String[]{ phoneNum },null);
        return cursor; //TODO miért érkezik ide kétszer a végrehajtás?
    }
}
