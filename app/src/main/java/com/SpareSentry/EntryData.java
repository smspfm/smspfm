package com.SpareSentry;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by App on 2018.03.12..
 */

public class EntryData {
        String FormatedDate;
        String Announcement;
    public EntryData(Long date, String announcement){
        DateFormat dateFormat = new SimpleDateFormat();
        FormatedDate = dateFormat.format(new Date(date)).toString();

        Announcement = announcement;
    }
}
