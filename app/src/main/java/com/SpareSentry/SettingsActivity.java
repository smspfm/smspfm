package com.SpareSentry;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.example.app.smspfm.R;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by App on 2018.10.08..
 */

public class SettingsActivity extends AppCompatActivity {
    Context context;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getBaseContext();

        setContentView(R.layout.activity_settings);

        findViewById(R.id.button_deleteCollectedData).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteFile("SavedData.cls");//TODO mindenhol átnevezni CollectedData-ra/ érdemes? negatívabb a kicsengése
            }
        });

        findViewById(R.id.button_deleteTemplate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteFile("Template.cls");
            }
        });
    }
}
