package com.SpareSentry;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by App on 2018.07.12..
 */

public class SavedData {
    float lastModified = 0.0f;
    float balance = 0.0f;
    float monthlyIncome = 0.0f;
    float monthlyIncomeLastMonth = 0.0f;
    float monthlyExpense = 0.0f;
    float monthlyExpenseLastMonth = 0.0f;

    List<MessageData> list_monthlyIncomes = new ArrayList<>();
    List<MessageData> list_lastMonthIncomes = new ArrayList<>();
    List<MessageData> list_oldIncomes = new ArrayList<>();

    List<MessageData> list_monthlyExpenses = new ArrayList<>(); //TODO saját objektum, ami megkapja a kursort, és feldolgozza
    List<MessageData> list_lastMonthExpenses = new ArrayList<>();
    List<MessageData> list_oldExpenses = new ArrayList<>();

    List<Entry> entries_balance = new ArrayList<>();
    public String lastExpenseDate;
    public String lastIncomeDate;
}
