package com.SpareSentry;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.app.smspfm.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

public class GraphActivity extends AppCompatActivity {
    BarEntry[] sourceBarEntries;
    BarData barData;
    boolean lineChart;
    Context context;
    int minHeight;
    int selectedSliceViewId = -1;
    int generatedChartId;
    Switch showNumsSwitch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        context = getApplicationContext();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;

        minHeight = (int) (height - (height/ 1.618));

        Gson gson = new Gson();
        if(getIntent().getExtras().getBoolean("lineChart")){
            lineChart = true;
            processLineChartEntries(gson.fromJson( getIntent().getExtras().getString("entries"), Entry[].class));
        } else {
            sourceBarEntries = gson.fromJson(getIntent().getExtras().getString("entries"), BarEntry[].class);
            processBarChartEntries(sourceBarEntries);
        }

        showNumsSwitch = findViewById(R.id.switch1);
        showNumsSwitch.setChecked(getIntent().getExtras().getBoolean("showNums"));
        if(showNumsSwitch.isChecked()){
            ShowNums();
        }else{
            HideNums();
        }
        showNumsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if(showNumsSwitch.isChecked()){
                    ShowNums();
                }else{
                    HideNums();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppSettings appSettings = Tools.readAppsettings(context);
        if (appSettings == null) appSettings = new AppSettings();
        appSettings.SwitchPos = false;
        Tools.writeAppsettings(context, appSettings);
    }

    private void processLineChartEntries(Entry[] entries_LineChart) {
        LineChart lineChart = new LineChart(context);
        generatedChartId =View.generateViewId();
        lineChart.setId(generatedChartId);
        lineChart.setMinimumHeight(minHeight);

        placeChartView(lineChart);

        List<Entry> temp_entryList = Arrays.asList(entries_LineChart);
        List<Entry> entryList = new ArrayList<>();

        float firstDayOfMonth = Tools.getFirstDayOfMonth(0);
        for( Entry entry : temp_entryList){
            if(entry.getX() > firstDayOfMonth){
                entryList.add(entry);
            }
        }
        Collections.sort(entryList,new EntryXComparator());

        LineDataSet dataSet = new LineDataSet(entryList, "Egyenleg");
        dataSet.setLineWidth(3);
        LineData lineData = new LineData(dataSet);
        lineChart.setData(lineData);
        lineChart.getLegend().setEnabled(false);
        lineChart.getDescription().setEnabled(false); //.setText("Egyenleg");

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd"); //("yyyy.MM.dd:hh:mm:ss");
                return sdf.format(new Date((long) value));
            }
        });
        //Jobboldali tengely eltüntetése
        lineChart.getAxisRight().setDrawLabels(false);
        lineChart.invalidate();
    }

    private void processBarChartEntries(BarEntry[] sourceBarEntries) {
        BarChart barChart = new BarChart(context);
        generatedChartId =View.generateViewId();
        barChart.setId(generatedChartId);
        barChart.setMinimumHeight(minHeight);

        placeChartView(barChart);

        List<BarEntry>  temp_barEntries = new ArrayList<>();
        List<BarEntry>  barEntries = new ArrayList<>();

        for( BarEntry entry : sourceBarEntries){ //TODO nem a sourcebar entries-t feldolgozni, mert az infocard-nál elrontja
            SimpleDateFormat sdf = new SimpleDateFormat("dd");
            BarEntry barEntry = entry.copy();
            barEntry.setY(entry.getY());//A copy nem működik jól.
            barEntry.setX( Float.parseFloat( sdf.format(new Date((long) entry.getX()))));
            temp_barEntries.add(barEntry);
        }

        int entriesSize = temp_barEntries.size();
        for(int i = 0; i < entriesSize; i = i){//TODO Stacked BarChart-ot csinálni, az azonos napiakból
            List<BarEntry> barEntryList = new ArrayList<>();
            barEntryList.add(temp_barEntries.get(i));
            for(int i2 = i+1; i2 < entriesSize; i2++){
                if(temp_barEntries.get(i).getX() == temp_barEntries.get(i2).getX() ) {//TODO második belső for ciklus, ami a többi sms-el hasonlítja össze
                    barEntryList.add(temp_barEntries.get(i2));
                }
            }
            if(barEntryList.size() > 1){ //TODO az utolsó egyezőnél mégegyszer végigmegy
                List<Float> floatList = new ArrayList<>();
                for (BarEntry barEntry : barEntryList){
                    try {
                        floatList.add(barEntry.getY());
                    } catch (Exception e) {}

                    try {
                        for (float f : barEntry.getYVals()) {
                            floatList.add(f);
                        }
                    } catch (Exception e) {}
                }

                float[] floats = new float[floatList.size()];
                for (int in = 0; in < floats.length; in++) {
                    floats[in] = floatList.get(in);
                    i++;
                }
                barEntries.add(new BarEntry(barEntryList.get(0).getX(),floats));
                i--;
            } else  barEntries.add(new BarEntry(temp_barEntries.get(i).getX(), new float[]{temp_barEntries.get(i).getY()}) );
            i++;
        }
        //Collections.addAll(expenseBarEntries, sourceBarEntries);
        //barChart.setDragEnabled(false);
        //barChart.setScaleEnabled(false);
        //barChart.getXAxis().setValueFormatter(new DateAxisValueFormatter(null));
        BarDataSet set = new BarDataSet(barEntries, "BarDataSet");
        //set.setColor(Color.parseColor("#FFC107"));
        set.setColors(ColorTemplate.MATERIAL_COLORS);

        barData = new BarData(set);
        barData.setBarWidth(0.9f); // set custom bar width
        barChart.setData(barData);
        barChart.setFitBars(true); // make the x-axis fit exactly all bars
        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawLabels(false);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        //Ne csak azt mutassa, ahol van érték, hanem az egész hónapot.
        xAxis.setAxisMaximum(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));

        final ConstraintLayout parent = findViewById(R.id.constraintLayout);
        /* TODO megcsinálni a kiválasztást alább
        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                CardView card = new CardView(context);
                Toolbar.LayoutParams params = new Toolbar.LayoutParams(
                        Toolbar.LayoutParams.MATCH_PARENT,
                        Toolbar.LayoutParams.MATCH_PARENT
                );
                params.bottomMargin = 16;
                card.setLayoutParams(params);
                card.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#80deea")));
                TextView tv = new TextView(context);
                tv.setLayoutParams(params);
                if(selectedSliceViewId < 0 ){
                    selectedSliceViewId = View.generateViewId();
                } else {
                    parent.removeView(findViewById(selectedSliceViewId));
                }
                card.setId(selectedSliceViewId);

                LinkedTreeMap barEntrieData = (LinkedTreeMap) e.getData();
                tv.setText(e.getY() + " HUF " ); //TODO már nincsennek ilyen adatai     + "\n" + barEntrieData.get("Comment") + "\n" + barEntrieData.get("FormatedDate"));
                tv.setTextAppearance(R.style.TextAppearance_AppCompat_Medium);
                //card.setContentPadding(0,32,0,32);
                card.setCardElevation(5);
                card.addView(tv);
                parent.addView(card, 1);
            }

            @Override
            public void onNothingSelected() {
                if(selectedSliceViewId > 0 ){
                    parent.removeView(findViewById(selectedSliceViewId));
                    selectedSliceViewId = -1;
                }
            }
        });
        */
        barChart.getLegend().setEnabled(false);
        barChart.getDescription().setEnabled(false);
        barChart.invalidate();

        CreateInfoCards(sourceBarEntries);
    }

    private void placeChartView(Chart chart) {
        //Y tengely felirata
        TextView textView_YaxisTitle = new TextView(context);
        textView_YaxisTitle.setText(getResources().getString(R.string.amount));//TODO a currency-re kicserélni
        textView_YaxisTitle.setId(View.generateViewId());

        //X tengely felirata
        TextView textView_XaxisTitle = new TextView(context);
        textView_XaxisTitle.setText(getResources().getString(R.string.day));
        textView_XaxisTitle.setId(View.generateViewId());

        //Vonal grafikon
        RelativeLayout relativeLayout = findViewById(R.id.relativeLayout_Graph);
        relativeLayout.addView(textView_YaxisTitle);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.BELOW, textView_YaxisTitle.getId());
        relativeLayout.addView(chart, layoutParams);

        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams2.addRule(RelativeLayout.BELOW, chart.getId());
        layoutParams2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        relativeLayout.addView(textView_XaxisTitle, layoutParams2);
    }

    private void CreateInfoCards(BarEntry[] barEntries) {
        int i = 0;
        LinearLayout parent = findViewById(R.id.linearLayout_Graph);
        for(BarEntry barEntrie : barEntries){
            CardView card = new CardView(context);
            Toolbar.LayoutParams params = new Toolbar.LayoutParams(
                    Toolbar.LayoutParams.MATCH_PARENT,
                    Toolbar.LayoutParams.MATCH_PARENT
            );
            params.bottomMargin = 16;
            card.setLayoutParams(params);


            TextView tv = new TextView(context);
            tv.setLayoutParams(params);
            String announcement = (String) barEntrie.getData();
            //TODO az alábbit megformázni, hogy Összeg: Közlemény: Dátum:
            tv.setText(barEntrie.getY() + " HUF " + "\n" + announcement + "\n" + new SimpleDateFormat().format(new Date( (long) barEntrie.getX() ))); //barEntrie.getX());//+ "\n" + barEntrieData.Comment); TODO curency használata
            tv.setTextAppearance(R.style.TextAppearance_AppCompat_Medium);
            //card.setContentPadding(0,32,0,32);
            card.setCardElevation(5);
            card.addView(tv);
            parent.addView(card, i);
            i++;
        }
    }

    private void ShowNums() {
        if(lineChart){
            LineChart lineChart = (LineChart) findViewById(generatedChartId);
            lineChart.getData().setDrawValues(true); //TODO null pointer exepction
            lineChart.getAxisLeft().setDrawLabels(true);
            lineChart.resetViewPortOffsets();
            lineChart.invalidate();
        } else {
            BarChart barChart = (BarChart) findViewById(generatedChartId);
            barData.setDrawValues(true); //TODO null pointer exepction
            barChart.getAxisLeft().setDrawLabels(true);
            barChart.resetViewPortOffsets();
            barChart.invalidate();
        }
        Tools.switchPosToFile(context, true);
    }

    private void HideNums() {
        if(lineChart){
            LineChart lineChart = (LineChart) findViewById(generatedChartId);
            lineChart.getData().setDrawValues(false); //TODO null pointer exepction
            lineChart.getAxisLeft().setDrawLabels(false);
            lineChart.resetViewPortOffsets();
            lineChart.invalidate();
        }else {
            BarChart barChart = (BarChart) findViewById(generatedChartId);
            barData.setDrawValues(false); //TODO null pointer exepction
            barChart.getAxisLeft().setDrawLabels(false);
            barChart.resetViewPortOffsets();
            barChart.invalidate();
        }
        Tools.switchPosToFile(context, false);
    }
}