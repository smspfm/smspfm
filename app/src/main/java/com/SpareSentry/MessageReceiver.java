package com.SpareSentry;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by App on 2018.07.13..
 */

public class MessageReceiver extends BroadcastReceiver {
    private static final String TAG = "MessageReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){ //TODO ellenőrízni a telefonszámot, null értékre figyelni
            Bundle bundle = intent.getExtras(); //---get the SMS message passed in---
            SmsMessage[] msgs = null;
            String msg_from;
            MessageProcessor messageProcessor = new MessageProcessor(context);
            Template template = Tools.readTemplate(context);
            if (bundle != null){ //---retrieve the SMS message received---
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    Log.d(TAG,"Number of messages received: "+ Integer.toString(msgs.length));
                    for(int i=0; i<msgs.length; i++){
                        msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        msg_from = msgs[i].getOriginatingAddress();
                        for (String phoneNumber : template.PhoneNumbers) {
                            if(phoneNumber. equals(msg_from)) {
                                String msgBody = msgs[i].getMessageBody();
                                Long TimestampMillis = msgs[i].getTimestampMillis();
                                Log.d(TAG, "Received a message, from: " + msg_from + ": " + msgBody);
                                if (msgBody != null) {
                                    messageProcessor.processMessage(MessageProcessor.getMessageType(msgBody, template), msgBody, TimestampMillis, template);
                                    messageProcessor.saveSaveData(context);
                                }
                            }
                        }
                    }
                }catch(Exception e){
                    Log.d(TAG,e.getMessage());         //TODO logolás
                }
            }
        }
    }
}
