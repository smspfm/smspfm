package com.SpareSentry;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.app.smspfm.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

/**
 * Created by App on 2018.08.27..
 */

public class DialogTrain extends DialogFragment {
    DialogTrainListener mListener;
    Long messageDate;

    public interface DialogTrainListener {
        public void AddDialogFinish(DialogFragment dialogFragment);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DialogTrainListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement ContinueDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()); //TODO amíg nincs megfelelő adat beírva a hozzáad gombot letiltani.
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_train, null);

        DataViewModel dataViewModel;
        dataViewModel = ViewModelProviders.of(this).get(DataViewModel.class);
        TextView MessageText = view.findViewById(R.id.textView_message);

        final Button buttonAdd = view.findViewById(R.id.button_add);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(isDialogCompleted(view)) buttonAdd.setEnabled(true);
            }
        };

        final EditText editText_Amount = view.findViewById(R.id.editText_Amount);
        editText_Amount.addTextChangedListener(textWatcher);

        final EditText editText_Announcement = view.findViewById(R.id.editText_Announcement);
        editText_Announcement.addTextChangedListener(textWatcher);

        final RadioGroup radioGroup  = view.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if( isDialogCompleted(view)) buttonAdd.setEnabled(true);
            }
        });

        buttonAdd.setEnabled(false);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {
                float amount = Float.parseFloat(editText_Amount.getText().toString());//TODO ellenőrízni a felhasználó inputjait.

                MessageData messageData = new MessageData(messageDate, amount, editText_Announcement.toString() );
                SavedData savedData = Tools.readSaveData(getContext());

                if(radioGroup.getCheckedRadioButtonId() == R.id.radioButton_income){
                    savedData.monthlyIncome += amount;
                    savedData.list_monthlyIncomes.add(messageData);
                    savedData.balance += amount;
                    savedData.lastIncomeDate = new SimpleDateFormat().format(new Date(messageDate));
                            /*
                            if(savedData.lastIncomeDate == null) {
                                savedData.lastIncomeDate = Long.valueOf(0L).toString();
                            }
                            if (Long.parseLong(savedData.lastIncomeDate) < messageDate) savedData.lastIncomeDate = messageDate.toString();*/
                }else {
                    savedData.monthlyExpense += amount;
                    savedData.list_monthlyExpenses.add(messageData);
                    savedData.balance -= amount;
                    savedData.lastIncomeDate = new SimpleDateFormat().format(new Date(messageDate));
                            /*
                            if(savedData.lastExpenseDate == null) {
                                savedData.lastExpenseDate = Long.valueOf(0L).toString();
                            }
                            if (Long.parseLong(savedData.lastExpenseDate) < messageDate) savedData.lastExpenseDate = messageDate.toString() + "00";*/
                }

                Tools.writeSaveData(getContext(), savedData);
                mListener.AddDialogFinish(DialogTrain.this);
                dismiss();
            }
        });

        view.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        builder.setView(view);
        return builder.create();
    }

    private boolean isDialogCompleted(View view) {
        RadioGroup radioGroup = view.findViewById(R.id.radioGroup);
        if(radioGroup.getCheckedRadioButtonId() == -1) return false;

        EditText editText_Amount = view.findViewById(R.id.editText_Amount);
        if(editText_Amount.getText().length() < 1) return false;

        EditText editText_Announcement = view.findViewById(R.id.editText_Announcement);
        if (editText_Announcement.getText().length() < 1) return false;

        return true;
    }

    public void show(FragmentManager fragmentManager, String tag) {
        super.show( fragmentManager, tag);
    }
}
