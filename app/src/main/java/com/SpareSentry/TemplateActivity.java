package com.SpareSentry;

import android.Manifest;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app.smspfm.R;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class TemplateActivity extends AppCompatActivity implements ContinueDialog.ContinueDialogListener {
    Context context;

    private static final int MY_PERMISSIONS_READ_SMS = 1;
    private static final int MY_PERMISSIONS_INTERNET = 2;
    private static final String TAG = "TemplateActivity";

    TextView textView_message;

    TextView textView_Announcement;
    EditText editText_trType;
    TextView textView_TrAmount;
    TextView textView_Balance;
    TextView textView_SmsType;

    ArrayList<TextView>  textViews = new ArrayList<>();

    Template template;

    List<Word> tr_types = new ArrayList<>();

    String delimiter = " ";

    List<String> currencys = new ArrayList<>();
    private List<String> messagesToCompare = new ArrayList<>();
    String bankName;

    enum tr_type {TRANSFER, OTHER, CREDIT, DEBIT, BUY}

    private tr_type choosedType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getBaseContext();
        template = Tools.readTempTemplate(context);

        //Az érkező inten-ből a bank telefonszámait illetve bank nevet kivesszük
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras != null) {
                messagesToCompare.addAll( extras.getStringArrayList("messagesToCompare"));
            }
        } else {
            messagesToCompare = (List<String>) savedInstanceState.getSerializable("messagesToCompare"); //TODO savedInstanceState átnézni hogy helyesen használtam-e
        }

        setContentView(R.layout.activity_template);

        setUpTextViews();
        setButtonOnClickListeners();

        textView_message.setText(messagesToCompare.get(0));
    }

    private void changeToMsgPickerActivity() { //Visszaváltunk az üzenet lista nézetre, és "nullázuk" a template rész dolgait.
        Tools.writeTempTemplate(context, template);
        Intent templateIntent = new Intent(context, MessagePickerActivity.class);
        templateIntent.putExtra("messagesToCompare",(Serializable) messagesToCompare);
        startActivity(templateIntent);
    }

    private void lookForAmounts() { //TODO nincs elég üzenet hiba dobása
        String originalMessage = textView_message.getText().toString();
        List<String> substrings =  Arrays.asList(originalMessage.split(" "));
        List<String> amounts = new ArrayList<>();
        for(String string : substrings){
            if(Pattern.compile("^\\d+(\\.|,)(\\d+(\\.|,)\\d+)*").matcher(string).find()){
                for(String currency : currencys){
                    if(substrings.get(substrings.indexOf(string) +1 ).toUpperCase().contains(currency)
                            //|| substrings.get(substrings.indexOf(string) -1 ).toUpperCase().contains(currency) )
                            // TODO az angolokra tekintettel, ezt is meg kéne oldani, előfordul, hogy dátumot is tévesen felismeri
                            ){
                        amounts.add(string);
                        Log.d(TAG, "lookForAmounts: found: " + string);
                    }
                }
            }
        }

        try {
            textView_TrAmount.setText(amounts.get(0)); //TODO a kisebb legyen a tranzakció, a nagyobb az egyenleg
        } catch (Exception e) {
            e.printStackTrace();//TODO logolás üzenetszöveggel
            textView_TrAmount.setText("");
        }

        try {
            textView_Balance.setText(amounts.get(1));
        } catch (Exception e) {
            e.printStackTrace();
            textView_Balance.setText("");
        }
    }

    //Ha nem teljes SmsPart-unk van felugró ablak megkérdezi, hogy akarjuk-e így folytatni
    @Override
    public void onDialogPositiveClick(android.app.DialogFragment dialog) {
        //Ha van kulcsszavunk, hozzáadjuk a szót, ha nem kihagyjuk
        if(editText_trType.getText().toString().length() > 0) {
            addSmsPartToTemplate();
        }
        if (!isTemplateCompleted()) {
            clearForm();
            changeToMsgPickerActivity();
        } else createTemplate();
    }

    private void setUpTextViews() {
        editText_trType = findViewById(R.id.editText_trType);
        textView_Announcement = findViewById(R.id.announcementTv);
        textView_SmsType = findViewById(R.id.smsTypeTV);
        textView_TrAmount = findViewById(R.id.tr_amountTv);
        textView_Balance = findViewById(R.id.balanceTv);

        textView_message = findViewById(R.id.textView_Msg);
        textView_message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(currencys.isEmpty()) {
                    currencys = Tools.getCurrencys();
                }
                lookForAmounts();
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        editText_trType.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textView_Announcement.setText(lookForComment());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        editText_trType.setText(lookForKeyWord());

        textViews.addAll(Arrays.asList(textView_Announcement, editText_trType, textView_TrAmount, textView_Balance));

        //Drag and drop műveletekre figyelés
        for (final TextView textView : textViews){
            textView.setOnDragListener(new View.OnDragListener() {
                @Override
                public boolean onDrag(View view, DragEvent dragEvent) {
                    if(dragEvent.getAction() == DragEvent.ACTION_DROP){
                        ClipData clipData = dragEvent.getClipData();
                        textView.setText( clipData.getItemAt(0).getText().toString()); //TODO A Word-ok templatehoz adásának újragondolása
                    }
                    return true;
                }
            });
        }

        textView_message.setCustomSelectionActionModeCallback(new ActionMode.Callback(){

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                menu.clear();
                MenuInflater inflater = actionMode.getMenuInflater();
                inflater.inflate(R.menu.select_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                String selection = getSelection(textView_message);

                switch (menuItem.getItemId()) {
                    case R.id.action_tr_type:
                        editText_trType.setText(selection);
                        actionMode.finish();
                        return true;
                    case R.id.action_amount:
                        textView_TrAmount.setText(selection);
                        actionMode.finish();
                        return true;
                    case R.id.action_balance:
                        textView_Balance.setText(selection);
                        actionMode.finish();
                        return true;
                    case R.id.action_announcement:
                        textView_Announcement.setText(selection);
                        actionMode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {}
        });
    }

    private String lookForKeyWord() {
        List<String> firstMessageSubstrings = Arrays.asList(messagesToCompare.get(0).split(" "));
        for(String string : firstMessageSubstrings){
           if(messagesToCompare.get(1).contains(string)){//TODO következő szó összevetése
               List<String> secondMessageSubstrings = Arrays.asList(messagesToCompare.get(1).split(" "));
               int index = 1;
               String result = string;
               while (secondMessageSubstrings.get( secondMessageSubstrings.indexOf(string) + index ).equals
                       (firstMessageSubstrings.get( firstMessageSubstrings.indexOf(string) + index )) ){
                   result += " " + firstMessageSubstrings.get( firstMessageSubstrings.indexOf(string) + index );
                   index++;
               }
               Log.d(TAG, "lookForKeyWord: found: " + result);
               return result;
           }
        }
        return " "; //Ne null-t adjunk vissza, hanem egy szóközt.
    }

    private String lookForComment() { //TODO rendberakni a tr_amount-ot
        List<String> currentMsgSubstrings =  Arrays.asList(Tools.normalize(messagesToCompare.get(0)).split(" "));
        String nextMsg = Tools.normalize(messagesToCompare.get(1));

        if (nextMsg != null){
            List<String> nextMsgSubstrings =  Arrays.asList(Tools.normalize(nextMsg).split(" "));
            int maxLength;
            if(currentMsgSubstrings.size() > nextMsgSubstrings.size()){
                maxLength = nextMsgSubstrings.size();
            } else maxLength = currentMsgSubstrings.size();
            String announcement = "";
            String trAmount = Tools.normalize(textView_TrAmount.getText().toString());
            String balance = Tools.normalize(textView_Balance.getText().toString());
            for (int i = 0; i < maxLength; i++){
                if(!currentMsgSubstrings.get(i).equals(nextMsgSubstrings.get(i))){
                    if(!currentMsgSubstrings.get(i).equals( trAmount) && !currentMsgSubstrings.get(i).equals(balance)){
                        announcement += " " + currentMsgSubstrings.get(i);
                    }
                }
            }
            return announcement;
        }
        return null;
    }

    private String geTextPos(String string) {
        String[] SmsSubStrings = textView_message.getText().toString().split(delimiter);

        String selectionFirstWord = string;
        try {//TODO logolás
            selectionFirstWord = string.trim().substring(0, string.trim().indexOf(delimiter));
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return Integer.toString(Arrays.asList(SmsSubStrings).indexOf(selectionFirstWord)); //TODO try catch-be bevonni
    }

    private String getSelection(TextView textView) {
        return  textView.getText().toString().substring(textView.getSelectionStart(), textView.getSelectionEnd()).trim();
    }

    private boolean isTemplateCompleted() { //TODO nem működik jól
        Field[] fields = template.getClass().getDeclaredFields();
        for(Field field: fields){
            field.setAccessible(true);
            if(getFieldObject(field) == null){
                try {
                    if(field.isSynthetic()) continue;
                    if(field.getName() == template.getClass().getField("Other").getName()){ //Ha csak egyéb SmsPart hiányzik, azzal nem foglalkozunk
                        continue;
                    } else return false;
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    Object getFieldObject(Field field){
        Object object;
        try {
            object = field.get(template);
            if(object == null){
                return null;
            } else return object;
        } catch (IllegalAccessException e) {
            return null; //TODO vmi rendes logolás
        }
    }

    private void addSmsPartToTemplate() {
        SmsPart smsPart = new SmsPart();
        smsPart.Balance = createWord((String) textView_Balance.getText().toString());
        smsPart.TrAmount = createWord((String) textView_TrAmount.getText().toString());
        smsPart.TrType = createWord((String) editText_trType.getText().toString());
        smsPart.Announcement = createWord((String) textView_Announcement.getText().toString());
        tr_types.add(smsPart.TrType);
        switch (choosedType) {
            case BUY:
                template.Buy = smsPart;
                break;
            case CREDIT:
                template.Credit = smsPart;
                break;
            case DEBIT:
                template.Debit = smsPart;
                break;
            case TRANSFER:
                template.Transfer = smsPart;
                break;
            case OTHER:
                template.Other = smsPart;
                break;
            default:
                throw new RuntimeException("Unknow message type");
        }
    }

    private void clearForm() {
        for (TextView t: textViews) {
            t.setText(null);
        }
        textView_SmsType.setText("");//külön van a többi textView-től
        choosedType = null;
    }

    private boolean isSmsPartCompleted() {
        if (isSmsTypeChoosed() && checkWords() ){//TODO textview villogtatása
            return true;
        } else return false;
    }

    private boolean checkWords() {
        for (TextView textView: textViews) {
            textView.invalidate();
            if(textView.getClass() == AppCompatEditText.class){
                EditText editText = (EditText) textView;
                String string = editText.getText().toString();
            }

            String string = textView.getText().toString();
            if (textView.getText().toString().equals("")) {
                ContinueDialog continueDialog = new ContinueDialog();
                continueDialog.show(getFragmentManager(),"ContinueDialog");
                return false;
            }
        }
        return true;
    }

    private boolean isSmsTypeChoosed() {
        if (choosedType != null){
            return true;
        } else {
            Toast.makeText(getBaseContext(), "Nincs SMS típus kiválasztva.", Toast.LENGTH_LONG).show(); //TODO @string-ből venni a szöveget
            return false;

        }
    }

    private Word createWord(String text) {
            Word word = new Word();
            word.Text = text;
            word.KeyWordPos = geTextPos(text); //TODO itt valami nem mindig jó
            return word;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) { //TODO ha nem kaptuk meg az engedélyt valamit csinálni
        if (requestCode == MY_PERMISSIONS_READ_SMS){ //TODO Ha nem kaptuk meg az engedélyt, akkor egy ablak, hogy miért kell, és újra megpróbáltatni
            changeToMsgPickerActivity();
        } else if (requestCode == MY_PERMISSIONS_INTERNET){
            //Felhasználó nem akar templatet csinálni, átvisszük a main activity-re manuális bevitellel
            Tools.writeSaveData(getApplicationContext(), new SavedData());
            Intent main = new Intent(getBaseContext(), MainActivity.class);
            main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(main);
        }
    }

    private void setButtonOnClickListeners() {
        //A csere gombra kattintva felcseréli a tranzakció és az egyenleg összegeit
        findViewById(R.id.switchAmounts).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CharSequence temp = textView_TrAmount.getText();
                textView_TrAmount.setText(textView_Balance.getText());
                textView_Balance.setText(temp);
            }
        });

        //Ok gombra kattintva
        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (isSmsPartCompleted()) {
                    addSmsPartToTemplate();                 //
                    if (!isTemplateCompleted()) {           // Ha itt módosítás van, azt át kell vezetni,
                        clearForm();                        // az onDialogPositiveClick metódusba is!
                        changeToMsgPickerActivity();              //
                    } else createTemplate();                //
                }
            }
        });


        //Üzenet típus választó gombok
        findViewById(R.id.buyButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosedType = tr_type.BUY;
                textView_SmsType.setText("Vásárlás");
            }
        });

        findViewById(R.id.creditButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosedType = tr_type.CREDIT;
                textView_SmsType.setText("Jóváírás");
            }
        });

        findViewById(R.id.debitButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosedType = tr_type.DEBIT;
                textView_SmsType.setText("Terhelés");
            }
        });
        findViewById(R.id.transferButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosedType = tr_type.TRANSFER;
                textView_SmsType.setText("Átutalás");
            }
        });

        findViewById(R.id.otherButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosedType = tr_type.OTHER;
                textView_SmsType.setText("Egyéb");
            }
        });
    }

    private void createTemplate(){
        templateAnonymisation();

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.INTERNET},
                    MY_PERMISSIONS_INTERNET);
        } else Tools.uploadTemplate(template);
        Tools.writeTemplate(context,template);
        deleteFile("tmpTemplate.cls");
        Intent main = new Intent(this, MainActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
    }

    //Összegek, és megjegyzések anonimizálása
    private void templateAnonymisation() {
        SmsPart[] smsParts = {template.Buy, template.Credit, template.Debit, template.Other, template.Transfer};
        for (SmsPart smsPart : smsParts){
            try {
                smsPart.Balance.Text = smsPart.Balance.Text.replaceAll("[0-9]", "#");
            } catch (NullPointerException e){
                //TODO logolás
            }
            try {
                smsPart.TrAmount.Text = smsPart.Balance.Text.replaceAll("[0-9]", "#");
            } catch (NullPointerException e){
                //TODO logolás
            }
            //megjegyzések anonimizálása
            try {
                smsPart.Announcement.Text = null;
            } catch (NullPointerException e){
                //TODO logolás
            }
        }
    }
}