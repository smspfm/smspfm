package com.SpareSentry;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app.smspfm.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

/**
 * Created by App on 2018.10.02..
 */

public class MessagePickerActivity extends AppCompatActivity {
    Context context;

    private static final int MY_PERMISSIONS_READ_SMS = 1;

    Template template;

    ArrayList<String> phoneNumberList = new ArrayList<>();
    private List<String> messagesToCompare = new ArrayList<>();
    String bankName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_template_message_picker);
        context = getBaseContext();

        DataViewModel dataViewModel;
        dataViewModel = ViewModelProviders.of(this).get(DataViewModel.class);

        template = Tools.readTempTemplate(context);
        if (template == null) {
            template = new Template();

            if (savedInstanceState == null) { //Tehát nem resetelt Activity
                Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    phoneNumberList.addAll(extras.getStringArrayList("phoneNumber"));
                    template.PhoneNumbers = phoneNumberList;

                    bankName = extras.getString("bankName");
                    Word wordBankName = new Word();
                    wordBankName.KeyWord = bankName;
                    template.Bankname = wordBankName;
                }
            }
        }
        findViewById(R.id.button_messagePickerOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.writeTemplate(context, template);//TODO tmpTemplate törlése
                Intent toMainIntent = new Intent(context, MainActivity.class);
                toMainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(toMainIntent);
            }
        });
    }

    @Override
    protected void onResume(){
        super.onResume();

        if (hasReadSmsPermission()) {
            fillLayoutWithMessages(); //TODO async metódusba kitenni, hogy ne akassza meg az UI-t
        } else askSmsPermission();
    }

    private void fillLayoutWithMessages() {
        LinearLayout linearLayout = findViewById(R.id.linearLayout_messageList);
        Cursor cursor = Tools.getBankSmsCursor(context, template.PhoneNumbers);
        messagesToCompare.clear();
        try {
            int i = 0;
            while (cursor.moveToNext()) {
                if(MessageProcessor.getMessageType(cursor.getString(2),template) == null) {
                    final CardView card = new CardView(context);
                    Toolbar.LayoutParams params = new Toolbar.LayoutParams(
                            Toolbar.LayoutParams.MATCH_PARENT,
                            Toolbar.LayoutParams.MATCH_PARENT
                    );
                    params.bottomMargin = 16;
                    card.setLayoutParams(params);


                    final TextView textView = new TextView(context);
                    textView.setLayoutParams(params);
                    textView.setText(cursor.getString(2));
                    textView.setTextAppearance(R.style.TextAppearance_AppCompat_Medium);
                    card.setCardElevation(5);
                    card.addView(textView);
                    card.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {//TODO a kiválasztott kártyákat lehessen nem kiválasztottá tenni
                            if(!card.isSelected()){
                                card.setSelected(true);
                                card.setElevation(15);
                                //card.setBackgroundColor(card.getContext().getResources().getColor(R.color.cardview_shadow_start_color));
                                messagesToCompare.add(textView.getText().toString()); //TODO ne lehessen 2x ugyanazt betenni a messagesToCompare-be
                                if (messagesToCompare.size() > 1) {
                                    Tools.writeTempTemplate(context, template);
                                    Intent templateIntent = new Intent(context, TemplateActivity.class);
                                    templateIntent.putExtra("messagesToCompare",(Serializable) messagesToCompare);
                                    startActivity(templateIntent);
                                }
                            }else{
                                card.setSelected(false);
                                card.setElevation(5);
                                for (String messageString : messagesToCompare){
                                    if (messageString.equals(textView.getText().toString())){
                                        messagesToCompare.remove(textView.getText().toString());
                                    }
                                }
                            }
                        }
                    });
                    linearLayout.addView(card, i);
                    i++;
                }
            }
        } finally {
            cursor.close();
        }
    }

    public boolean hasReadSmsPermission(){//TODO futásközbeni engedély kérése magyarázó szöveg
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        else return true;
    }

    private void askSmsPermission(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS},
                MY_PERMISSIONS_READ_SMS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults)
    {
        if (requestCode == MY_PERMISSIONS_READ_SMS) {
            fillLayoutWithMessages();
        }//TODO Ha nem kaptuk meg az engedélyt, akkor egy ablak, hogy miért kell, és újra megpróbáltatni
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putStringArrayList("phoneNumberList", phoneNumberList);
        savedInstanceState.putString("bankName", bankName);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        phoneNumberList = savedInstanceState.getStringArrayList("phoneNumberList");
        bankName = savedInstanceState.getString("bankName");
    }
}
