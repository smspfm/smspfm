package com.SpareSentry;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app.smspfm.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.MessageListViewHolder> {
    Context context;
    List<String> messageList;

    public class MessageListViewHolder extends RecyclerView.ViewHolder{
        public TextView message;
        public View viewForeground, viewCredit, viewDebit;

        public MessageListViewHolder(@NonNull View itemView) {
            super(itemView);
            message = itemView.findViewById(R.id.textView_message);
            viewForeground = itemView.findViewById(R.id.view_foreground);
            viewCredit = itemView.findViewById(R.id.view_credit);
            viewDebit = itemView.findViewById(R.id.view_debit);
        }
    }

    public MessageListAdapter(Context context, List<String> messageList){
        this.context = context;
        this.messageList = messageList;
    }

    @NonNull
    @Override
    public MessageListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View messageListItemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message_list, parent, false);

        return new MessageListViewHolder(messageListItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageListViewHolder holder, int position) {
        holder.message.setText(messageList.get(position));
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }
}
